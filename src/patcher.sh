#!/usr/bin/bash
#!/bin/bash

# (C) Copyright 2022 Venkatesh Mishra
# Patcher - GNU GPL v3
# Patcher has moved to https://github.com/Netwrk-3/patcher
# Patcher is now backed by Netwrk-3 Inc.

###### OLD PATCHER CODE #########
mem_cleaner() {
    echo 3 > /proc/sys/vm/drop_caches
}

disk_cleaner() {
    rm -rf /tmp/*
    rm ~./cache/*
}

msg()
{
  echo "$(tput bold; tput setaf 2)[+] ${*}$(tput sgr0)"
}

patch_vuln() {
  sudo sysctl kernel.modules_disabled=1
  sudo sysctl kernel.pid_max = 65536
  sudo sysctl kernel.core_uses_pid = 1
  # CVE-2022-0847 patch
  sudo sysctl kernel.unprivileged_bpf_disabled=1
  sudo sysctl kernel.unprivileged_userns_clone=0
  sudo sysctl kernel.kptr_restrict=2
  sudo sysctl lvm.unprivileged_userfaultfd=0
  
  sudo sysctl -a
  sudo sysctl -A
  sudo sysctl mib
  sudo sysctl net.ipv4.conf.all.rp_filter
  sudo sysctl -a --pattern 'net.ipv4.conf.(eth|wlan)0.arp'
}

check_priv()
{
  if [ "$(id -u)" -ne 0 ]; then
    err "you must be root to run patcher"
  fi
}

main() {
    check_priv
    msg 'Patcher v0.1-alpha'
    msg 'reducing memory usage'
    mem_cleaner
    msg 'cleaning temp files and cache'
    msg 'patching kernel...'
    patch_vuln
    disk_cleaner
}
