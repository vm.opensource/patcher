#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define SFTWRE_NAME Patcher
#define version 0.1

// memory usage reducer
int mur() {
    int core = system("echo 3 > /proc/sys/vm/drop_caches");
    if(core == NULL){
        fprintf(stderr,"Error while loading MURA");
        return 1;
    } else {
        return 0;
    }
    return 0;
}

int main(int short argc, char * const argv[]) {
    int short i;
    for(i=0;i<1;i++){
        printf("Reducing Memory used\n");
        mur();

    }
    return 0;
}
